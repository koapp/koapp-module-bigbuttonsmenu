# demo Module
===================================

![demo-popover](images/popover.png)

### Description

This menu has XXL buttons. you'll be able to add image to each of the options.
  
   
### Configuration
  
To configure this module, choose an image for each option on your menu. If no image is selected, the button will have no image and only the background color will appear.


### Images
- [Logo](images/logo.png)
- [Screenshot](images/screenshot01.png)


### Details:

- Author: King of app
- Version: 0.0.1
- Homepage: 