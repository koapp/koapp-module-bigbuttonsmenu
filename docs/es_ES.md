### Description

Este menú se caracteriza por tener unos botones XXL que permiten añadir imágenes individuales para cada opción.
  
  
### Configuration
Para configurar este módulo elige una imagen para cada opción de tu menú. Si no hay imagen seleccionada ese botón no tendrá imagen y aparecerá únicamente el color de fondo.

